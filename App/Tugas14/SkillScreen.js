import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  ScrollView,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import datum from './skillData.json';

function Skills({skillName, categoryName, iconName, percentageProgress}) {
  return (
    <View style={styles.skills}>
      <Icon
        name={iconName}
        color={'#003366'}
        size={80}
        style={{flex: 1, marginLeft: 10}}
      />
      <View style={{flexDirection: 'column', marginHorizontal: 10}}>
        <Text style={{fontWeight: 'bold', fontSize: 24}}>{skillName}</Text>
        <Text style={{fontWeight: 'bold', fontSize: 16, color: '#3EC6FF'}}>
          {categoryName}
        </Text>
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 48,
            textAlign: 'right',
            color: 'white',
          }}>
          {percentageProgress}
        </Text>
      </View>
      <Icon name="chevron-right" size={80} style={{flex: 1}} />
    </View>
  );
}

export default class Note extends React.Component {
  render() {
    const data = datum.items;
    return (
      <View style={styles.container}>
        <View>
          <Image style={styles.logo} source={require('./logo_putih.png')} />
          <Text
            style={{
              textAlign: 'right',
              width: '97%',
              color: '#3EC6FF',
            }}>
            PORTFOLIO
          </Text>
        </View>
        <View style={styles.bio}>
          <Icon name="account-circle" size={26} color={'#3EC6FF'} />
          <View style={{marginLeft: 11}}>
            <Text style={{fontSize: 12}}>Hai,</Text>
            <Text style={{fontSize: 16, color: '#003366'}}>
              Raditya Firman S
            </Text>
          </View>
        </View>
        <Text style={{fontSize: 36, color: '#003366', marginTop: 16}}>
          SKILL
        </Text>
        <View style={{height: 4, backgroundColor: '#3EC6FF', width: '100%'}} />
        <View style={styles.category}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Text style={styles.categories}>Library/Framework</Text>
            <Text style={styles.categories}>Bahasa Pemrograman</Text>
            <Text style={styles.categories}>Teknologi</Text>
          </ScrollView>
        </View>
        <FlatList
          style={styles.skill}
          keyExtractor={data => data.id}
          data={data}
          renderItem={({item}) => (
            <Skills
              skillName={item.skillName}
              categoryName={item.categoryName}
              iconName={item.iconName}
              percentageProgress={item.percentageProgress}
            />
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    height: 640,
    padding: 16,
  },
  logo: {
    marginLeft: 'auto',
    marginRight: 0,
  },
  bio: {
    flexDirection: 'row',
    height: 33,
    width: 147,
    marginTop: 3,
    alignItems: 'center',
  },
  category: {
    marginTop: 10,
    flexDirection: 'row',
  },
  categories: {
    borderRadius: 4,
    height: 32,
    backgroundColor: '#3EC6FF',
    color: '#003366',
    padding: 8,
    fontWeight: 'bold',
    lineHeight: 14,
    marginHorizontal: 6,
  },
  skill: {
    marginVertical: 10,
  },
  skills: {
    height: 129,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#B4E9FF',
    marginBottom: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
