import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SkillScreen from './SkillScreen';
import AddScreen from './AddScreen';
import ProjectScreen from './ProjectScreen';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

function TabsScreen ({navigation}) {
  return (
      <Tabs.Navigator>
        <Tabs.Screen name='Skills' component={SkillScreen} options={{title: 'Skills'}} />
        <Tabs.Screen name='Project' component={ProjectScreen} options={{title: 'Projects'}} />
        <Tabs.Screen name='Add Project' component={AddScreen} options={{title: 'Add Project'}} />
      </Tabs.Navigator>
  );
}

function DrawerScreen ({navigation}) {
  return (
      <Drawer.Navigator>
        <Drawer.Screen name='About' component={AboutScreen} options={{title: 'About'}} />
        <Drawer.Screen name='Tabs' component={TabsScreen} options={{title: 'Tabs'}} />
      </Drawer.Navigator>
  );
}

function Login ({navigation}){
  return (
    <>
      <LoginScreen />
      <Button title='Drawer' onPress={() => navigation.navigate('About')} />
    </>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Login' component={Login} />
        <Stack.Screen name='About' component={DrawerScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
