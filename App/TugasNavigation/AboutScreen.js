import React from 'react';
import {StyleSheet, Text, View, Dimensions, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function AboutScreen() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.judul}>Tentang Saya</Text>
        <Icon
          style={styles.photo}
          name="account-circle"
          size={200}
          color={'gray'}
        />
        <Text style={styles.nama}>Raditya Firman Syaputra.</Text>
        <Text style={styles.job}>React Native Developer</Text>
        <View style={{paddingHorizontal: 8, marginTop: 16}}>
          <View style={styles.box}>
            <Text
              style={{
                fontSize: 18,
                color: '#003366',
                marginBottom: 8,
              }}>
              Portofolio
            </Text>
            <View
              style={{
                width: '100%',
                borderColor: '#003366',
                borderWidth: 1,
              }}
            />
            <View
              style={{flexDirection: 'row', marginTop: 20, marginBottom: 17}}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  alignItems: 'center',
                }}>
                <Icon name="gitlab" size={50} color={'#3EC6FF'} />
                <Text>@radityafiqa4</Text>
              </View>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  alignItems: 'center',
                }}>
                <Icon name="github-circle" size={50} color={'#3EC6FF'} />
                <Text>@radityafiqa4</Text>
              </View>
            </View>
          </View>
          <View style={styles.box}>
            <Text
              style={{
                fontSize: 18,
                color: '#003366',
                marginBottom: 8,
              }}>
              Hubungi Saya
            </Text>
            <View
              style={{
                width: '100%',
                borderColor: '#003366',
                borderWidth: 1,
              }}
            />
            <View style={{flexDirection: 'column', marginVertical: 16}}>
              <View
                style={{
                  paddingHorizontal: 100,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}>
                <Icon name="facebook" color={'#3EC6FF'} size={40} />
                <Text
                  style={{
                    marginLeft: 19,
                    color: '#003366',
                    fontSize: 16,
                    fontWeight: 'bold',
                  }}>
                  Raditya Firman Syaputra
                </Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 100,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}>
                <Icon name="instagram" color={'#3EC6FF'} size={40} />
                <Text
                  style={{
                    marginLeft: 19,
                    color: '#003366',
                    fontSize: 16,
                    fontWeight: 'bold',
                  }}>
                  @sekutumu
                </Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 100,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                }}>
                <Icon name="twitter" color={'#3EC6FF'} size={40} />
                <Text
                  style={{
                    marginLeft: 19,
                    color: '#003366',
                    fontSize: 16,
                    fontWeight: 'bold',
                  }}>
                  Raditya Firman Syaputra
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    height: Dimensions.get('window').height,
  },
  judul: {
    fontSize: 36,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 64,
    color: '#003366',
  },
  photo: {
    marginTop: 12,
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  nama: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#003366',
    textAlign: 'center',
    marginTop: 24,
  },
  job: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#3EC6FF',
    textAlign: 'center',
    marginTop: 8,
  },
  box: {
    paddingHorizontal: 8,
    paddingVertical: 5,
    backgroundColor: '#EFEFEF',
    borderRadius: 16,
    width: '100%',
    marginBottom: 9,
  },
});
