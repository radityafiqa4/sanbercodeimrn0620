const Soal1 = () => {
  console.log("Soal 1");
  var word = "JavaScript";
  var second = "is";
  var third = "awesome";
  var fourth = "and";
  var fifth = "I";
  var sixth = "love";
  var seventh = "it!";
  console.log(
    `${word} ${second} ${third} ${fourth} ${fifth} ${sixth} ${seventh}\n\n`
  );
};

const Soal2 = () => {
  console.log("Soal 2");
  var s = "I am going to be React Native Developer";
  var exampleFirstWord = s[0];
  var secondWord = s[2] + s[3];
  var thirdWord = s[5] + s[6] + s[7] + s[8] + s[9];
  var fourthWord = s[11] + s[12];
  var fiftWord = s[14] + s[15];
  var sixthWord = s[17] + s[18] + s[19] + s[20] + s[21];
  var seventhWord =
    s[30] + s[31] + s[32] + s[33] + s[34] + s[35] + s[36] + s[37] + s[38];
  console.log("First Word: " + exampleFirstWord);
  console.log("Second Word: " + secondWord);
  console.log("Third Word: " + thirdWord);
  console.log("Fourth Word: " + fourthWord);
  console.log("Fifth Word: " + fiftWord);
  console.log("Sixth Word: " + sixthWord);
  console.log("Seventh Word: " + seventhWord + "\n\n");
};

const Soal3 = () => {
  console.log("Soal 3");
  var sentence2 = "wow JavaScript is so cool";
  var exampleFirstWord2 = sentence2.substring(0, 3);
  var secondWord2 = sentence2.substring(4, 14);
  var thirdWord2 = sentence2.substring(15, 17);
  var fourthWord2 = sentence2.substring(18, 20);
  var fifthWord2 = sentence2.substring(21, 25);
  console.log("First Word: " + exampleFirstWord2);
  console.log("Second Word: " + secondWord2);
  console.log("Third Word: " + thirdWord2);
  console.log("Fourth Word: " + fourthWord2);
  console.log("Fifth Word: " + fifthWord2 + "\n\n");
};

const Soal4 = () => {
  console.log("Soal 4");
  var sentence3 = "wow JavaScript is so cool";
  var FirstWord3 = sentence3.substring(0, 3);
  var secondWord2 = sentence3.substring(4, 14);
  var thirdWord2 = sentence3.substring(15, 17);
  var fourthWord2 = sentence3.substring(18, 20);
  var fifthWord2 = sentence3.substring(21, 25);
  console.log(`First Word: ${FirstWord3} with length: ${FirstWord3.length}`);
  console.log(`Second Word: ${secondWord2} with length: ${secondWord2.length}`);
  console.log(`Third Word: ${thirdWord2} with length: ${thirdWord2.length}`);
  console.log(`Fourth Word: ${fourthWord2} with length: ${fourthWord2.length}`);
  console.log(`Fifth Word: ${fifthWord2} with length: ${fifthWord2.length}`);
};

Soal1();
Soal2();
Soal3();
Soal4();
