const readline = require("readline-sync");

const ifelse = () => {
  const nama = readline.question("Masukan Nama anda ? ");
  if (nama !== "") {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
    const animals = ["Penyihir", "Guard", "Warewolf"];
    const index = readline.keyInSelect(
      animals,
      "Pilih peranmu untuk memulai game!"
    );
    if (animals[index] === "Penyihir") {
      console.log(
        `Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`
      );
    } else if (animals[index] === "Guard") {
      console.log(
        `Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf.`
      );
    } else if (animals[index] === "Warewolf") {
      console.log(`"Halo Werewolf ${nama}, Kamu akan memakan mangsa setiap malam!" 
        `);
    }
  } else {
    console.log("Nama harus diisi!");
  }
};

const switchcase = () => {
  const hari = readline.question("Masukan Tanggal (1-31) : ");
  const bulan = readline.question("Masukan Bulan (1-12): ");
  const tahun = readline.question("Masukan Tahun (1900-2200): \n");
  if (
    hari > 0 &&
    hari < 31 &&
    bulan > 0 &&
    bulan <= 12 &&
    tahun >= 1900 &&
    tahun <= 2200
  ) {
    switch (bulan) {
      case "1": {
        console.log(`${hari} Januari ${tahun}`);
        break;
      }
      case "2": {
        console.log(`${hari} Februari ${tahun}`);
        break;
      }
      case "3": {
        console.log(`${hari} Maret ${tahun}`);
        break;
      }
      case "4": {
        console.log(`${hari} April ${tahun}`);
        break;
      }
      case "5": {
        console.log(`${hari} Mei ${tahun}`);
        break;
      }
      case "6": {
        console.log(`${hari} Juni ${tahun}`);
        break;
      }
      case "7": {
        console.log(`${hari} Juli ${tahun}`);
        break;
      }
      case "8": {
        console.log(`${hari} Agustus ${tahun}`);
        break;
      }
      case "9": {
        console.log(`${hari} September ${tahun}`);
        break;
      }
      case "10": {
        console.log(`${hari} Oktober ${tahun}`);
        break;
      }
      case "11": {
        console.log(`${hari} November ${tahun}`);
        break;
      }
      case "12": {
        console.log(`${hari} Desember ${tahun}`);
        break;
      }
    }
  } else {
    console.log("Masukan Tanggal Yang Valid");
  }
};

console.log("Tugas Conditional");
const materi = ["If / Else", "Switch Case"];
const index = readline.keyInSelect(materi, "Pilih Materi Tugas!");
if (materi[index] == "If / Else") {
  ifelse();
} else if (materi[index] == "Switch Case") {
  switchcase();
}
