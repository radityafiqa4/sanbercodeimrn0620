import React, { Component } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

export default class VideoItem extends Component {
  render() {
    let video = this.props.video;

    return (
      <View style={styles.container}>
        <Image
          source={{ uri: video.snippet.thumbnails.medium.url }}
          style={styles.image}
        />
        <View style={styles.descContainer}>
          <Image
            source={{ uri: "https://randomuser.me/api/portraits/men/0.jpg" }}
            style={styles.ImageProfile}
          />
          <View style={styles.videoDetails}>
            <Text numberOfLines={2} style={styles.videoTitle}>
              {video.snippet.title}
            </Text>
            <Text style={styles.videoStat}>
              {video.snippet.channelTitle +
                " . " +
                video.statistics.viewCount.slice(0, 2).split("").join(",") +
                "M views 3 months ago"}
            </Text>
          </View>
          <TouchableOpacity>
            <Icon name="more-vert" size={20} color="#999999" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
  image: {
    height: 200,
  },
  ImageProfile: { width: 50, height: 50, borderRadius: 25 },
  descContainer: {
    flexDirection: "row",
    paddingTop: 10,
  },
  videoDetails: {
    paddingHorizontal: 15,
    paddingLeft: 10,
    flex: 1,
  },
  videoTitle: {
    fontSize: 16,
    color: "#3C3C3C",
  },
  videoStat: {
    fontSize: 15,
    paddingTop: 3,
  },
});
