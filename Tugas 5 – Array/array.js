const ranges = require("./range.js");

const soal1 = () => {
  const range = (start, end) => {
    if (start > end) {
      return ranges(start, end);
    } else {
      return ranges(start, end).sort(function (a, b) {
        return b - a;
      });
    }
  };
  console.log(range(10, 1));
};

const soal2 = () => {
  const rangeWithStep = (start, end, step = 1) => {
    if (start > end) {
      return ranges(start, end, step);
    } else {
      return ranges(start, end, step).sort(function (a, b) {
        return b - a;
      });
    }
  };
  console.log(rangeWithStep(10, 100, 10));
};

const soal3 = () => {
  const sum = (start, end, step = 1) => {
    if (start > end) {
      return ranges(start, end, step).reduce((a, b) => a + b, 0);
    } else {
      return ranges(start, end, step)
        .sort(function (a, b) {
          return b - a;
        })
        .reduce((a, b) => a + b, 0);
    }
  };
  console.log(sum(10, 100, 10));
};

const soal4 = () => {
  const dataHandling = (array) => {
    for (let i = 0; i < array.length; i++) {
      console.log(`Nomor ID: ${array[i][0]}`);
      console.log(`Nama Lengkap: ${array[i][1]}`);
      console.log(`TTL: ${array[i][2]} ${array[i][3]}`);
      console.log(`Hobi: ${array[i][4]}\n`);
    }
  };

  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
  ];
  dataHandling(input);
};

const soal5 = () => {
  const balikKata = (str) => str.split("").reverse().join("");
  console.log(balikKata("Kasur Rusak"));
};

const soal6 = () => {
  const input1 = [
    "0001",
    "Roman Alamsyah",
    "Bandar Lampung",
    "21/05/1989",
    "Membaca",
  ];
  input1.splice(1, 1, "Roman Alamsyah Elsharawy");
  input1.splice(2, 1, "Provinsi Bandar Lampung");
  input1.splice(4, 1, "Pria", "SMA Internasional Metro");
  console.log(input1);
  const tgl = input1[3].split("/");
  switch (tgl[1]) {
    case "01": {
      console.log(`Januari`);
      break;
    }
    case "02": {
      console.log(`Februari`);
      break;
    }
    case "03": {
      console.log(`Maret`);
      break;
    }
    case "04": {
      console.log(`April`);
      break;
    }
    case "05": {
      console.log(`Mei`);
      break;
    }
    case "06": {
      console.log(`Juni`);
      break;
    }
    case "07": {
      console.log(`Juli`);
      break;
    }
    case "08": {
      console.log(`Agustus`);
      break;
    }
    case "09": {
      console.log(`September`);
      break;
    }
    case "10": {
      console.log(`Oktober`);
      break;
    }
    case "11": {
      console.log(`November`);
      break;
    }
    case "12": {
      console.log(`Desember`);
      break;
    }
  }
  const tgldesc = tgl.sort(function (a, b) {
    return b - a;
  });
  console.log(tgldesc);
  console.log(input1[3].split("/").join("-"));
  console.log(input1[1].toString().slice(0, 15));
};
