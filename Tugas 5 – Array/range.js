/**
 * Source by Lodash.Range
 */

var INFINITY = 1 / 0,
  MAX_SAFE_INTEGER = 9007199254740991,
  MAX_INTEGER = 1.7976931348623157e308,
  NAN = 0 / 0;

var funcTag = "[object Function]",
  genTag = "[object GeneratorFunction]",
  symbolTag = "[object Symbol]";

var reTrim = /^\s+|\s+$/g;

var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;

var reIsBinary = /^0b[01]+$/i;

var reIsOctal = /^0o[0-7]+$/i;

var reIsUint = /^(?:0|[1-9]\d*)$/;

var freeParseInt = parseInt;

var objectProto = Object.prototype;

var objectToString = objectProto.toString;

var nativeCeil = Math.ceil,
  nativeMax = Math.max;

function baseRange(start, end, step, fromRight) {
  var index = -1,
    length = nativeMax(nativeCeil((end - start) / (step || 1)), 0),
    result = Array(length);

  while (length--) {
    result[fromRight ? length : ++index] = start;
    start += step;
  }
  return result;
}

function createRange(fromRight) {
  return function (start, end, step) {
    if (step && typeof step != "number" && isIterateeCall(start, end, step)) {
      end = step = undefined;
    }

    start = toFinite(start);
    if (end === undefined) {
      end = start;
      start = 0;
    } else {
      end = toFinite(end);
    }
    step = step === undefined ? (start < end ? 1 : -1) : toFinite(step);
    return baseRange(start, end, step, fromRight);
  };
}

function isIndex(value, length) {
  length = length == null ? MAX_SAFE_INTEGER : length;
  return (
    !!length &&
    (typeof value == "number" || reIsUint.test(value)) &&
    value > -1 &&
    value % 1 == 0 &&
    value < length
  );
}

function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (
    type == "number"
      ? isArrayLike(object) && isIndex(index, object.length)
      : type == "string" && index in object
  ) {
    return eq(object[index], value);
  }
  return false;
}

function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

function isFunction(value) {
  var tag = isObject(value) ? objectToString.call(value) : "";
  return tag == funcTag || tag == genTag;
}

function isLength(value) {
  return (
    typeof value == "number" &&
    value > -1 &&
    value % 1 == 0 &&
    value <= MAX_SAFE_INTEGER
  );
}

function isObject(value) {
  var type = typeof value;
  return !!value && (type == "object" || type == "function");
}

function isObjectLike(value) {
  return !!value && typeof value == "object";
}

function isSymbol(value) {
  return (
    typeof value == "symbol" ||
    (isObjectLike(value) && objectToString.call(value) == symbolTag)
  );
}

function toFinite(value) {
  if (!value) {
    return value === 0 ? value : 0;
  }
  value = toNumber(value);
  if (value === INFINITY || value === -INFINITY) {
    var sign = value < 0 ? -1 : 1;
    return sign * MAX_INTEGER;
  }
  return value === value ? value : 0;
}

function toNumber(value) {
  if (typeof value == "number") {
    return value;
  }
  if (isSymbol(value)) {
    return NAN;
  }
  if (isObject(value)) {
    var other = typeof value.valueOf == "function" ? value.valueOf() : value;
    value = isObject(other) ? other + "" : other;
  }
  if (typeof value != "string") {
    return value === 0 ? value : +value;
  }
  value = value.replace(reTrim, "");
  var isBinary = reIsBinary.test(value);
  return isBinary || reIsOctal.test(value)
    ? freeParseInt(value.slice(2), isBinary ? 2 : 8)
    : reIsBadHex.test(value)
    ? NAN
    : +value;
}

var range = createRange();

module.exports = range;
