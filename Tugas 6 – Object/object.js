const arrayToObject = (arr) => {
  const initialValue = {};
  var now = new Date();
  var thisYear = now.getFullYear();
  if (arr.length != 0) {
    return arr.reduce((obj, item) => {
      item[3] = item[3] < thisYear ? thisYear - item[3] : "Invalid Birth Year";
      return {
        ...obj,
        [item[0] + " " + item[1]]: item,
      };
    }, initialValue);
  }
  return "";
};

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
console.log(arrayToObject(people));

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
console.log(arrayToObject(people2));
console.log(arrayToObject([]));

const shoppingTime = (memberid = "", money) => {
  if ((memberid != "") & (money >= 50000)) {
    const buyed = [];
    let changemoney = money;

    const tuku = (stuff, price) => {
      changemoney = changemoney - price;
      buyed.push(stuff);
    };

    money >= 1500000 ? tuku("Sepatu brand Stacattu", 1500000) : money;
    money >= 500000 ? tuku("Baju brand Zoro", 500000) : money;
    money >= 250000 ? tuku("Baju brand H&N", 250000) : money;
    money >= 175000 ? tuku("Sweater brand Uniklooh", 175000) : money;
    money >= 50000 ? tuku("Casing Handphone", 50000) : money;

    return {
      memberid: memberid,
      money: money,
      listPurchased: [buyed],
      changemoney: changemoney,
    };
  }

  return memberid == ""
    ? "Mohon maaf, toko X hanya berlaku untuk member saja"
    : "Mohon maaf, uang tidak cukup";
};

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());

const naikAngkot = (arrPenumpang) => {
  const rute = ["A", "B", "C", "D", "E", "F"];
  const res = [];

  for (let i = 0; i < arrPenumpang.length; i++) {
    const name = arrPenumpang[i][0];
    const from = arrPenumpang[i][1];
    const to = arrPenumpang[i][2];
    const price = (rute.indexOf(to) - rute.indexOf(from)) * 2000;
    res.push({
      penumpang: name,
      naikDari: from,
      tujuan: to,
      bayar: price,
    });
  }
  return res;
};

console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
console.log(naikAngkot([]));
