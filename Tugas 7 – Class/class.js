class Animal {
  constructor(name) {
    this._name = name;
  }

  get name() {
    return this._name;
  }

  get legs() {
    return 4;
  }
  set legs(i) {
    this.legs = i;
  }

  get cold_blooded() {
    return false;
  }

  set cold_blooded(a) {
    this.cold_blooded = a;
  }
}

var sheep = new Animal("shaun");
console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

class Ape extends Animal {
  yell() {
    console.log("Auooo");
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

class Frog extends Animal {
  jump() {
    console.log("hop hop");
  }
}

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

class Clock {
  constructor({ template }) {
    this.template = template;
  }
  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;
    let output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }
  stop() {
    clearInterval(this.timer);
  }
  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
