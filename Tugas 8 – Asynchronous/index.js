var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

readBooks(
  10000,
  books[(i = 0)],
  (callback = (cb) => {
    i++;
    if (i < books.length) {
      readBooks(cb, books[i], callback);
    }
  })
);
