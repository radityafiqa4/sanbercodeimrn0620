var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
let i = 0;
function readBook(sisa = 10000) {
  if (i < books.length) {
    readBooksPromise(sisa, books[i])
      .then((sisaWaktu) => {
        i++;
        readBook(sisaWaktu);
      })
      .catch((sisaWaktu) => {
        i++;
        readBook(sisaWaktu);
      });
  }
}

readBook();
