const soal1 = (i = 2, reverse = false) => {
  console.log(`LOOPING PERTAMA`);
  while (true) {
    if (reverse == false) {
      console.log(`${i} - I Love Coding`);
      i += 2;
      if (i == 20) {
        console.log(`${i} - I Love Coding`);
        console.log(`LOOPING KEDUA`);
        reverse = true;
      }
    } else {
      console.log(`${i} - I will become a mobile developer`);
      i -= 2;
      if (i == 0) {
        break;
      }
    }
  }
};

const soal2 = () => {
  for (let i = 1; i <= 20; i++) {
    if (i % 3 == 0) {
      console.log(`${i} - I Love Coding`);
    } else if (i % 2 == 1) {
      console.log(`${i} - Santai`);
    } else if (i % 2 == 0) {
      console.log(`${i} - Berkualitas`);
    }
  }
};

const soal3 = (panjang = 8, lebar = 4, tag = "#") => {
  let tags = tag;
  for (let i = 1; i <= panjang; i++) {
    tags = tags + tag;
  }
  for (let i = 1; i <= lebar; i++) {
    console.log(tags);
  }
};

const soal4 = (tinggi = 7, tag = "#") => {
  for (let i = 0; i < tinggi; i++) {
    let tags = tag;
    for (let ii = 0; ii < i; ii++) {
      tags = tags + tag;
    }
    console.log(tags);
  }
};

const soal5 = (panjang = 8, tag = "# # # #") => {
  for (let i = 1; i <= panjang; i++) {
    if (i % 2 == 1) {
      console.log(` ${tag}`);
    } else if (i % 2 == 0) {
      console.log(`${tag} `);
    }
  }
};
