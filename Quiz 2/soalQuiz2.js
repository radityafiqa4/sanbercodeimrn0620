class Score {
  constructor(subject, points, email) {
    this.subject = subject;
    this.points = points;
    this.email = email;
  }

  average(points) {
    let isNum = (n) => (isNaN(n) ? 0 : n);
    const average =
      points.reduce((a, b) => isNum(a) + isNum(b)) / (points.length - 1);
    return average.toFixed(1);
  }

  viewScores(data, subject) {
    let res = new Array();
    let idx = data[0].findIndex((value) => value === subject);
    for (let i = 1; i < data.length; i++) {
      res.push({
        email: data[i][0],
        subject,
        points: data[i][idx],
      });
    }
    console.log(res);
  }

  recapScore(data) {
    for (let i = 1; i < data.length; i++) {
      let average = this.average(data[i]);
      let predikat;
      if (average > 90) {
        predikat = "honour";
      } else if (average > 80) {
        predikat = "graduate";
      } else if (average > 70) {
        predikat = "participant";
      }
      console.log(
        `${i}. Email : ${data[i][0]}\nRata-Rata: ${average}\nPredikat: ${predikat}\n`
      );
    }
  }
}

const hasil = new Score();

const data = [
  ["email", "quiz-1", "quiz-2", "quiz-3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93],
];

// TEST CASE
hasil.viewScores(data, "quiz-1");
hasil.viewScores(data, "quiz-2");
hasil.viewScores(data, "quiz-3");
hasil.recapScore(data);
